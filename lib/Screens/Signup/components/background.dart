import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      // Here i can use size.width but use double.infinity because both work as a same
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            top: -40,
            //   left: 0,
            //   child: Image.asset(
            //     "assets/images/signup_top.png",
            //     width: size.width * 0.10,
            //   ),
            // ),
            child: Image.asset(
              "assets/images/drawable-xxxhdpi-icon.png",
              width: size.width * 0.40,
              height: size.height * 0.8,
            ),
            // Positioned(
            //   bottom: 0,
            //   left: 0,
            //   child: Image.asset(
            //     "assets/images/main_bottom.png",
            //     width: size.width * 0.25,
            //   ),
          ),
          child,
        ],
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/cr-bg.png'), fit: BoxFit.cover),
      ),
    );
  }
}
